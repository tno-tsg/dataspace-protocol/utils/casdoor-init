FROM node:20-alpine as build

WORKDIR /app
COPY package* /app/
RUN npm install
COPY . .
RUN npm run tsc

FROM node:20-alpine

WORKDIR /app
COPY package* /app/
RUN npm install --production
COPY --from=build /app/dist /app
ENTRYPOINT [ "node", "index.js" ]