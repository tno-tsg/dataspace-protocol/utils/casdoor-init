import { Application } from "casdoor-nodejs-sdk/lib/cjs/application";
import { Organization } from "casdoor-nodejs-sdk/lib/cjs/organization";
import { Role } from "casdoor-nodejs-sdk/lib/cjs/role";
import { User } from "casdoor-nodejs-sdk/lib/cjs/user";

type RecursivePartial<T> = {
  [P in keyof T]?: T[P] extends (infer U)[]
    ? RecursivePartial<U>[]
    : T[P] extends object | undefined
    ? RecursivePartial<T[P]>
    : T[P];
};

export function organizationTemplate(
  id: string,
  name: string,
  owner: string,
  overwrite: Record<string, any> = {}
): Organization {
  const result: RecursivePartial<Organization> = {
    owner: owner,
    name: id,
    createdTime: "2024-01-24T09:02:25+01:00",
    displayName: name,
    websiteUrl: "https://tno-tsg.gitlab.io",
    favicon:
      "https://tno-tsg.gitlab.io/assets/images/favicon/favicon-32x32.png",
    passwordType: "argon2id",
    passwordSalt: "",
    passwordOptions: [],
    countryCodes: [],
    defaultAvatar: "https://cdn.casbin.org/img/casbin.svg",
    defaultApplication: "",
    tags: [],
    languages: ["en"],
    masterPassword: "",
    initScore: 0,
    enableSoftDeletion: false,
    isProfilePublic: true,
    accountItems: [
      {
        name: "Organization",
        visible: true,
        viewRule: "Public",
        modifyRule: "Admin",
      },
      {
        name: "ID",
        visible: true,
        viewRule: "Public",
        modifyRule: "Immutable",
      },
      {
        name: "Name",
        visible: true,
        viewRule: "Public",
        modifyRule: "Admin",
      },
      {
        name: "User type",
        visible: true,
        viewRule: "Public",
        modifyRule: "Admin",
      },
      {
        name: "Password",
        visible: true,
        viewRule: "Self",
        modifyRule: "Self",
      },
      {
        name: "Email",
        visible: true,
        viewRule: "Public",
        modifyRule: "Self",
      },
      {
        name: "Roles",
        visible: true,
        viewRule: "Public",
        modifyRule: "Immutable",
      },
      {
        name: "Properties",
        visible: true,
        viewRule: "Admin",
        modifyRule: "Admin",
      },
      {
        name: "Is admin",
        visible: true,
        viewRule: "Admin",
        modifyRule: "Admin",
      },
      {
        name: "Is forbidden",
        visible: true,
        viewRule: "Admin",
        modifyRule: "Admin",
      },
      {
        name: "Is deleted",
        visible: true,
        viewRule: "Admin",
        modifyRule: "Admin",
      },
    ],
    ...overwrite
  };
  return result as any as Organization;
}

export function userTemplate(
  id: string,
  name: string,
  organization: string,
  password: string,
  isAdmin: boolean,
  properties: Record<string, any> = {},
  overwrite: Record<string, any> = {}
): User {
  const result: RecursivePartial<User> = {
    owner: organization,
    name: id,
    type: "normal-user",
    password: password,
    displayName: name,
    avatar: "https://cdn.casbin.org/img/casbin.svg",
    email: `noreply-${(Math.random() + 1).toString(36).substring(2)}@dataspac.es`,
    phone: "",
    address: [],
    affiliation: "",
    tag: "staff",
    score: 2000,
    ranking: 2,
    isAdmin: isAdmin,
    isForbidden: false,
    isDeleted: false,
    signupApplication: "",
    properties: properties,
    ...overwrite
  };
  return result as any as User;
}

export function applicationTemplate(
  id: string,
  name: string,
  owner: string,
  organization: string,
  clientId: string,
  clientSecret: string,
  redirectUris: string[],
  overwrite: Record<string, any> = {}
): Application {
  const result: RecursivePartial<Application> | { signinMethods: any[] } = {
    organization: organization,
    owner: owner,
    name: id,
    displayName: name,
    logo: "https://tno-tsg.gitlab.io/assets/images/TSG-Logo-Black.svg",
    homepageUrl: "",
    cert: "cert-built-in",
    enablePassword: true,
    enableSignUp: true,
    clientId: clientId,
    clientSecret: clientSecret,
    tokenFormat: "JWT-Custom",
    grantTypes: ["id_token","refresh_token","client_credentials","password"],
    tokenFields: ["Owner","Name","DisplayName","Email","Roles","Properties"],
    providers: [
      {
        name: "provider_captcha_default",
        canSignUp: false,
        canSignIn: false,
        canUnlink: false,
        prompted: false,
        alertType: "None",
      },
    ],
    signinMethods: [
      {
        name: "Password",
        displayName: "Password",
        rule: "All",
      },
    ],
    signupItems: [
      {
        name: "ID",
        visible: false,
        required: true,
        prompted: false,
        rule: "Random",
      },
      {
        name: "Username",
        visible: true,
        required: true,
        prompted: false,
        rule: "None",
      },
      {
        name: "Password",
        visible: true,
        required: true,
        prompted: false,
        rule: "None",
      },
      {
        name: "Confirm password",
        visible: true,
        required: true,
        prompted: false,
        rule: "None",
      },
      {
        name: "Email",
        visible: true,
        required: true,
        prompted: false,
        rule: "No verification",
      },
      {
        name: "Agreement",
        visible: true,
        required: true,
        prompted: false,
        rule: "None",
      },
    ],
    redirectUris: redirectUris,
    expireInHours: 24,
    refreshExpireInHours: 168,
    ...overwrite
  };
  return result as any as Application;
}

export function roleTemplate(
  id: string,
  name: string,
  organization: string,
  users: string[],
  overwrite: Record<string, any> = {}
): Role {
  const result: RecursivePartial<Role> = {
    displayName: name,
    isEnabled: true,
    name: id,
    owner: organization,
    roles: [],
    users: users.map((u) => `${organization}/${u}`),
    ...overwrite
  };
  return result as any as Role;
}
