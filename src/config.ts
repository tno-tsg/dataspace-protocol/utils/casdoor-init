
export interface InitConfig {
  url: string
  rootPassword: string | undefined
  namespace: string | undefined
  organization: OrganizationConfig
  users: UserConfig[]
  applications: ApplicationConfig[]
  roles: RoleConfig[]
}

export interface OrganizationConfig {
  id: string
  name: string
  overwrite: Record<string, any> | undefined
}

export interface UserConfig {
  username: string
  password: string | undefined
  isAdmin: boolean | undefined
  secretName: string | undefined
  roles: string[]
  properties: Record<string, any> | undefined
  overwrite: Record<string, any> | undefined
}

export interface ApplicationConfig {
  id: string
  name: string
  redirectUris: string[]
  clientId: string | undefined
  clientSecret: string | undefined
  users: UserConfig[]
  overwrite: Record<string, any> | undefined
}

export interface RoleConfig {
  id: string
  name: string
  users: string[]
  overwrite: Record<string, any> | undefined
}