import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { applicationTemplate, organizationTemplate, roleTemplate, userTemplate } from './templates';
import crypto from "crypto";
import { KubeConfig, CoreV1Api, PatchUtils } from '@kubernetes/client-node';
import { InitConfig } from './config';
import { parse } from 'yaml';
import { readFileSync } from 'fs';

interface CasdoorResponse<T = any> {
  status: string
  msg: string
  data: T
}

async function run() {
  try {
    const initConfig = parse(readFileSync('./config.yaml', 'utf8')) as InitConfig
    const kubeConfig = new KubeConfig();
    kubeConfig.loadFromDefault();

    const namespace = initConfig.namespace || kubeConfig.contexts.find(c => c.name === kubeConfig.currentContext)?.namespace
    if (!namespace) {
      throw Error('No namespace configured and namespace discovery failed');
    }
    const k8sApi = kubeConfig.makeApiClient(CoreV1Api);
    let cookie = await resetPassword(initConfig.url, initConfig.rootPassword, k8sApi, namespace);

    const client = axios.create({
      baseURL: `${initConfig.url}/api`,
      timeout: 60000,
      headers: {
        Cookie: cookie
      }
    });
    await add(client, '/add-organization', organizationTemplate(initConfig.organization.id, initConfig.organization.name, 'admin', initConfig.organization.overwrite));

    console.log(`Added organization ${initConfig.organization.name}`);
    for (const user of initConfig.users) {
      const userPassword = user.password || crypto.randomBytes(20).toString('hex');
      await add(client, '/add-user', userTemplate(user.username, user.username, initConfig.organization.id, userPassword, user.isAdmin || false, user.properties, user.overwrite));
      console.log(`Added user ${user.username}`);
      if (user.secretName) {
        await applySecret(k8sApi, user.secretName, namespace, {
          username: user.username,
          password: userPassword
        });
        console.log(`Stored user ${user.username} credentials in secret ${user.secretName}`);
      } else if (!user.password) {
        console.log(`\n\nWARNING: Created user without secret name or predefined password\n  Username: ${user.username}\n  Password: ${userPassword}\n\n`);
      }
    }
    for (const application of initConfig.applications) {
      const clientId = crypto.randomBytes(10).toString('hex');
      const clientSecret = crypto.randomBytes(20).toString('hex');
      for (const applicationUser of application.users) {
        const userPassword = applicationUser.password || crypto.randomBytes(20).toString('hex');
        await add(client, '/add-user', userTemplate(applicationUser.username, applicationUser.username, initConfig.organization.id, userPassword, false, applicationUser.properties, applicationUser.overwrite));
        console.log(`Added user ${applicationUser.username}`);
        await applySecret(k8sApi, applicationUser.secretName || `${applicationUser.username}-auth-secret`, namespace, {
          clientId: clientId, 
          clientSecret: clientSecret,
          username: applicationUser.username,
          password: userPassword
        });
        console.log(`Stored application and user credentials in secret ${applicationUser.secretName || `${applicationUser.username}-auth-secret`}`);
      }
      await add(client, '/add-application', applicationTemplate(application.id, application.name, 'admin', initConfig.organization.id, clientId, clientSecret, application.redirectUris, application.overwrite));
      console.log(`Added application ${application.name}`);
    }
    for (const role of initConfig.roles) {
      const users = new Set([
        ...[
          ...initConfig.applications.flatMap(a => a.users), 
          ...initConfig.users
        ]
          .filter(u => u.roles?.includes(role.id))
          .map(u => u.username),
        ...(role.users ?? [])
      ])
      
      await add(client, '/add-role', roleTemplate(role.id, role.name, initConfig.organization.id, [...users], role.overwrite))
      console.log(`Added role ${role.name}`);
    }
    console.log('Completed initialization');
  } catch (err) {
    console.error(`Initialization failed`)
    console.error((err as Error).message)
    process.exitCode = 1
    await new Promise(resolve => setTimeout(resolve, 30000))
  }
}

async function applySecret(api: CoreV1Api, name: string, namespace: string, secrets: Record<string, string>) {
  const encodedSecrets: Record<string, string> = {}
  for (const key in secrets) {
    encodedSecrets[key] = Buffer.from(secrets[key]).toString('base64');
  }
  try {
    return await api.patchNamespacedSecret(name, namespace, {
      apiVersion: 'v1',
      kind: 'Secret',
      type: 'Opaque',
      metadata: {
        name: name,
        namespace: namespace,
      },
      data: encodedSecrets
    }, undefined, undefined, 'casdoor-init', undefined, undefined, {
      headers: {
        'Content-Type': PatchUtils.PATCH_FORMAT_APPLY_YAML
      }
    })
  } catch (err) {
    if (axios.isAxiosError(err) && err.response) {
        throw Error(`Error in applying secret: ${err.response.status}\n${JSON.stringify(err.response.data, null, 2)}`)
    } else {
      throw Error(`Error in applying secret: ${err}`)
    }
  } 
}

async function add<T extends {owner: string, name: string}>(client: AxiosInstance, url: string, obj: T): Promise<CasdoorResponse> {
  let response;
  try {
    response = await client.post<T, AxiosResponse<CasdoorResponse>>(url, obj, {
      params: {
        id: `${obj.owner}/${obj.name}`
      }
    });
  } catch (err) {
    if (axios.isAxiosError(err) && err.response) {
      throw Error(`Error in adding casdoor entity (${url}): ${err.response.status}\n${JSON.stringify(err.response.data, null, 2)}`)
    } else {
      throw Error(`Error in adding casdoor entity (${url}): ${err}`)
    }
  }
  if (response.data.status !== 'ok') {
    throw Error(response.data.msg);
  }
  return response.data;
}

async function resetPassword(url: string, rootPassword: string | undefined, api: CoreV1Api, namespace: string) {
  let loginResponse;
  for (let i = 0; i < 5; i++) {
    try {
      loginResponse = await axios.post(`${url}/api/login`, {
        "application":"app-built-in",
        "organization":"built-in",
        "username":"admin",
        "autoSignin":true,
        "password":"123",
        "signinMethod":"Password",
        "type":"login"
      });
    } catch (err) {
      if (i == 4) {
        if (axios.isAxiosError(err) && err.response) {
          throw Error(`Error in logging in: ${err.response.status}\n${JSON.stringify(err.response.data, null, 2)}`)
        } else {
          throw Error(`Error in logging in: ${err}`)
        }
      } else {
        if (axios.isAxiosError(err) && err.response) {
          console.log(`Error connecting to casdoor (${err.response.status}), retrying in 5 seconds`);
        } else {
          console.log(`Error connecting to casdoor, retrying in 5 seconds`);
        }
        await new Promise(resolve => setTimeout(resolve, 5000));
      }
    }
  }
  if (!loginResponse) {
    throw Error('Unexpected error during login');
  }
  if (loginResponse.data.status !== "ok") {
    throw Error(`Error logging in: ${JSON.stringify(loginResponse.data)}`);
  }
  const cookie = loginResponse.headers["set-cookie"];
  if (!cookie) {
    throw Error('No login cookie')
  }
  console.log('Successfully logged in');
  const casdoorCookie = cookie[0].split(';')[0];

  const newPassword = rootPassword || crypto.randomBytes(20).toString('hex');

  let data = new FormData();
  data.append('userOwner', 'built-in');
  data.append('userName', 'admin');
  data.append('oldPassword', '');
  data.append('newPassword', newPassword);

  const updatePasswordResponse = await axios.post(`${url}/api/set-password`, data, {
    headers: {
      Cookie: casdoorCookie,
      "Content-Type": "multipart/form-data"
    }
  });
  if (updatePasswordResponse.data.status !== "ok") {
    throw Error(`Error updating password: ${JSON.stringify(updatePasswordResponse.data)}`)
  }
  console.log('Successfully updated admin password');
  await applySecret(api, 'casdoor-root-user', namespace, {
    username: 'admin',
    password: newPassword
  });
  console.log('Stored admin password credentials in "casdoor-root-user" secret');
  return casdoorCookie;
}

run();